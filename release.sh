#!/bin/bash

version=$(cat physics-tools/module.json | jq -r .version)
patch=$(echo $version | sed 's/.*\.//')
minor=$(echo $version | sed 's/\.[0-9]*$//')

if [ -z $1 ]
then
  patch=$((patch + 1))
fi
version=${minor}.${patch}
echo $version

sed -i "s/\"version\": \".*\",/\"version\": \"$version\",/" physics-tools/module.json
sed -i "s#/raw/v[0-9.]*/#/raw/v${version}/#" physics-tools/module.json
mkdir -p release

zip -r release/physics-tools.zip ./physics-tools
cp physics-tools/module.json release/module.json
